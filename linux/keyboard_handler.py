import re
import os
import evdev
from basic_evdev_handler import Handler

class Consts:
    CTRL_UP_CODE = "KEY_LEFTCTRL"
    ALT_UP_CODE = "KEY_LEFTALT"

class CouldntFindKeyboard(Exception):
    pass

def get_keyboard_event_device():
    device_output = os.popen("grep -A5 -i keyboard /proc/bus/input/devices | grep -i handlers").read()
    event_num = re.search("event[0-9]+", device_output)
    if not event_num:
        raise CouldntFindKeyboard('Can\'t find Keyboard device!')
    return f"/dev/input/{event_num.group(0)}"

class KeyboardHandler(Handler):
    def __init__(self, *kargs, **kwargs):
        self.currently_pressed_keys = set()
        super().__init__(get_keyboard_event_device(), *kargs, **kwargs)
    
    def start_listening_async(self):
        return super()._start_listening_async()
    
    def handle_key_input_event(self, event):
        if event.keystate == event.key_down:
            self.handle_key_down(event)
        elif event.keystate == event.key_up:
            self.handle_key_up(event)

    def handle_key_down(self, event):
        self.currently_pressed_keys.add(event.keycode)
        if {Consts.CTRL_UP_CODE, Consts.ALT_UP_CODE}.issubset(self.currently_pressed_keys):
            self.down_function()
    
    def handle_key_up(self, event):
        if event.keycode in self.currently_pressed_keys:
            self.currently_pressed_keys.remove(event.keycode)
        if event.keycode == Consts.ALT_UP_CODE:
            self.up_function()
