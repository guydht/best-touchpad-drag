from copy import deepcopy
from functools import wraps

import evdev
import pynput
from pynput.keyboard import Controller as KeyboardController
from pynput.mouse import Controller as MouseController


class Handler:
    ecodes = evdev.ecodes
    mouse_controller = MouseController()
    keyboard_controller = KeyboardController()

    def __init__(self, device_path, mouse_down, mouse_up):
        self.down_function = self.__handler_wrapper(mouse_down)
        self.up_function = self.__handler_wrapper(mouse_up)
        self.device = evdev.InputDevice(device_path)
        self.__current_complete_events = list()
        super().__init__()

    async def _start_listening_async(self):
        async for event in self.device.async_read_loop():
            self.handle_event(event)

    def start_listening(self):
        for event in self.device.read_loop():
            self.handle_event(event)

    def __handler_wrapper(self, func):
        @wraps(func)
        def wrap(*args, **kwargs):
            return func(self, *args, **kwargs)

        return wrap

    def handle_event(self, event):
        event = evdev.categorize(event)
        event_mapper = {
            evdev.AbsEvent: self.handle_abs_input_event,
            evdev.KeyEvent: self.handle_key_input_event,
            evdev.SynEvent: self.handle_syn_input_event,
            evdev.RelEvent: self.handle_rel_input_event,
            evdev.InputEvent: self.handle_input_event
        }
        event_type = type(event)
        if event_type in event_mapper:
            event_mapper[event_type](event)
        if event_type is evdev.SynEvent:
            self.handle_complete_events(self.__current_complete_events)
            self.__current_complete_events.clear()
        else:
            self.__current_complete_events.append(event)

    def handle_complete_events(self, events):
        """
        Functions to be called for each "syn" event - and the parameters are all the events that happened
        """
        pass

    def handle_abs_input_event(self, event):
        pass

    def handle_key_input_event(self, event):
        pass

    def handle_syn_input_event(self, event):
        pass

    def handle_rel_input_event(self, event):
        pass

    def handle_input_event(self, event):
        pass

    def key_down(self, key):
        self.keyboard_controller.press(pynput.keyboard.Key[key])

    def key_up(self, key):
        self.keyboard_controller.release(pynput.keyboard.Key[key])

    def mouse_down(self, button=1):
        try:
            self.mouse_controller.press(pynput.mouse.Button(button))
        except:
            pass

    def mouse_up(self, button=1):
        try:
            self.mouse_controller.release(pynput.mouse.Button(button))
        except:
            pass


class Point:
    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y
        super().__init__()

    def clear(self):
        self.x = None
        self.y = None

    def __setitem__(self, key, value):
        if key == Handler.ecodes.ABS_MT_POSITION_X:
            self.x = value
        elif key == Handler.ecodes.ABS_MT_POSITION_Y:
            self.y = value

    def __repr__(self):
        return f"{{x: {self.x}, y: {self.y}}}"


class BasicTouchpadHandler(Handler):
    @staticmethod
    def DEFAULT_FINGER_POSITION_OBJECT():
        return [Point() for _ in range(5)]

    __last_finger_position = getattr(
        DEFAULT_FINGER_POSITION_OBJECT, '__func__')()
    current_key_codes = set()
    num_of_fingers = 0

    @staticmethod
    def get_last_fingers_position():
        return deepcopy(BasicTouchpadHandler.__last_finger_position)

    @staticmethod
    def get_current_fingers_position():
        obj = deepcopy(BasicTouchpadHandler.__last_finger_position)
        mapper = {
            Handler.ecodes.BTN_TOOL_FINGER: 0,
            Handler.ecodes.BTN_TOOL_DOUBLETAP: 1,
            Handler.ecodes.BTN_TOOL_TRIPLETAP: 2,
            Handler.ecodes.BTN_TOOL_QUADTAP: 3,
            Handler.ecodes.BTN_TOOL_QUINTTAP: 4
        }
        for key, index in mapper.items():
            if not key in BasicTouchpadHandler.current_key_codes:
                obj[index].clear()
        return obj

    def handle_complete_events(self, events):
        slot = 0
        for event in events:
            if type(event) is evdev.AbsEvent:
                if event.event.code == Handler.ecodes.ABS_MT_SLOT:
                    slot = event.event.value
                elif event.event.code in (Handler.ecodes.ABS_MT_POSITION_X, Handler.ecodes.ABS_MT_POSITION_Y):
                    BasicTouchpadHandler.__last_finger_position[slot][event.event.code] = event.event.value

    def handle_key_input_event(self, event):
        if event.keystate == evdev.KeyEvent.key_up and event.event.code in BasicTouchpadHandler.current_key_codes:
            BasicTouchpadHandler.current_key_codes.remove(event.event.code)
        elif event.keystate == evdev.KeyEvent.key_down:
            BasicTouchpadHandler.current_key_codes.add(event.event.code)
        BasicTouchpadHandler.num_of_fingers = len(
            BasicTouchpadHandler.current_key_codes)

    def get_max_dimensions(self):
        capabilities = self.device.capabilities()[self.ecodes.EV_ABS]
        point = Point()
        for code, data in capabilities:
            if code == self.ecodes.ABS_MT_POSITION_X:
                point.x = data.max
            elif code == self.ecodes.ABS_MT_POSITION_Y:
                point.y = data.max
        return point
