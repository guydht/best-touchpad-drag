import time
import asyncio

def main():
    try:
        from touchpad_handler import TouchpadHandler
        touchpad = TouchpadHandler(mouse_down, mouse_up)
        asyncio.ensure_future(touchpad.start_listening_async())
        loop = asyncio.get_event_loop()
        loop.run_forever()
    except Exception as e:
        print(e, type(e))
        time.sleep(1)
        main()

def mouse_down(handler):
    handler.mouse_down()

def mouse_up(handler):
    handler.mouse_up()


if __name__ == "__main__":
    main()

