import re
import os
from basic_evdev_handler import BasicTouchpadHandler


class Consts:
    THREE_FINGERS_EVENT_CODE = BasicTouchpadHandler.ecodes.BTN_TOOL_TRIPLETAP
    TWO_FINGERS_EVENT_CODE = BasicTouchpadHandler.ecodes.BTN_TOOL_DOUBLETAP
    ONE_FINGER_EVENT_CODE = BasicTouchpadHandler.ecodes.BTN_TOOL_FINGER
    TOUCH_EVENT_CODE = BasicTouchpadHandler.ecodes.BTN_TOUCH
    edge_threshold_percentage = 6


class CouldntFindTouchpad(Exception):
    pass


def get_touchpad_event_device():
    device_output = os.popen(
        "grep -A5 -i touchpad /proc/bus/input/devices | grep -i handlers").read()
    event_num = re.search("event[0-9]+", device_output)
    if not event_num:
        raise CouldntFindTouchpad('Can\'t find Touchpad device!')
    return f"/dev/input/{event_num.group(0)}"


class TouchpadHandler(BasicTouchpadHandler):
    def __init__(self, *kargs, **kwargs):
        self.gesture_start_time = None
        self.two_fingers_start_time = None
        self.did_mouse_down = False
        self.max_num_of_fingers_in_gesture = 0
        super().__init__(get_touchpad_event_device(), *kargs, **kwargs)

    def start_listening_async(self):
        return super()._start_listening_async()

    def handle_key_up_input_event(self, event):
        if event.event.code == Consts.ONE_FINGER_EVENT_CODE and \
                Consts.TOUCH_EVENT_CODE not in self.current_key_codes and \
                self.did_mouse_down and \
                not self.is_in_edge():
            self.up_function()
            self.did_mouse_down = False
        elif event.event.code == Consts.TWO_FINGERS_EVENT_CODE and \
                Consts.ONE_FINGER_EVENT_CODE in self.current_key_codes and \
                self.did_mouse_down:
            self.up_function()
            self.did_mouse_down = False
        elif event.event.code == Consts.TWO_FINGERS_EVENT_CODE and \
                Consts.ONE_FINGER_EVENT_CODE in self.current_key_codes and \
                event.event.timestamp() - self.gesture_start_time > 0.1 and \
                event.event.timestamp() - self.two_fingers_start_time < 0.2:
            self.down_function()
            self.did_mouse_down = True

    def handle_key_down_input_event(self, event):
        if event.event.code == Consts.TOUCH_EVENT_CODE:
            self.gesture_start_time = event.event.timestamp()
        elif event.event.code == Consts.TWO_FINGERS_EVENT_CODE:
            self.two_fingers_start_time = event.event.timestamp()
        elif event.event.code != Consts.ONE_FINGER_EVENT_CODE:
            self.did_mouse_down = False

    def handle_key_input_event(self, event):
        if event.keystate == event.key_down:
            self.handle_key_down_input_event(event)
        elif event.keystate == event.key_up:
            self.handle_key_up_input_event(event)
        if type(event.keycode) is list:
            event.keycode = event.keycode[0]
        super().handle_key_input_event(event)

    def is_in_edge(self):
        max = self.get_max_dimensions()
        current = self.get_current_fingers_position()[0]
        if not current.x or not max.x:
            return False
        percentage_x = current.x / max.x * 100
        percentage_y = current.y / max.y * 100
        for percentage in (percentage_x, percentage_y):
            if percentage < Consts.edge_threshold_percentage or \
                    100 - percentage < Consts.edge_threshold_percentage:
                return True
        return False
