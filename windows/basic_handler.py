"""
A *VERY* disgusting class, using ctypes and user32 dll to listen to raw touchpad events.

"""
from contextlib import suppress
import ctypes
from time import time
import ctypes
from ctypes import wintypes
import c_structs
from abc import ABC, abstractmethod
from setup_message_window import create_message_window, user32, DefWindowProcW, kernel32
from multiprocessing import Process
from fasteners import InterProcessLock
from pathlib import Path

WM_QUIT = 0x0012
WM_TOUCH = 0x0240
WM_INPUT = 0x00FF
WM_KEYUP = 0x0101
WM_CHAR = 0x0102
WM_LBUTTONDOWN = 0x0201
WM_RBUTTONDOWN = 0x0204
WM_LBUTTONUP = 0x0202
WM_RBUTTONUP = 0x0205

HidP_Input = 0x0
HID_USAGE_DIGITIZER_CONTACT_ID = 0x51
HID_USAGE_DIGITIZER_CONTACT_COUNT = 0x54

HID_USAGE_PAGE_DIGITIZER = 0x0D
HID_USAGE_GENERIC_TOUCHPAD = 0x0D

RI_MOUSE_LEFT_BUTTON_DOWN = 0x01
RI_MOUSE_LEFT_BUTTON_UP = 0x02

RIDEV_NOLEGACY = 0x00000030
RIDEV_INPUTSINK = 0x00000100
RIDEV_CAPTUREMOUSE = 0x00000200

RID_HEADER = 0x10000005
RID_INPUT = 0x10000003

RIM_TYPEMOUSE = 0
RIM_TYPEKEYBOARD = 1
RIM_TYPEHID = 2

PM_NOREMOVE = 0x0000
RIDI_PREPARSEDDATA = 0x20000005

hid_parser = ctypes.WinDLL("hid")


class Handler(ABC):
    def run(self):
        lock_file = Path(f"{__file__}.lock")
        lock = InterProcessLock(lock_file)
        acquired = lock.acquire(False)
        if acquired:
            try:
                create_message_window(self._window_event_callback, self._window_blocking_events_callback)
            except BaseException:
                with suppress(Exception):
                    lock.release()
                    lock_file.unlink()
                raise

    def stop(self, *args):
        self.process.kill()

    def _window_blocking_events_callback(self, nCode, wParam, lParam):
        if nCode >= 0:
            if wParam == WM_LBUTTONDOWN:
                return self.on_blocking_pointer_down()
            elif wParam == WM_LBUTTONUP:
                return self.on_blocking_pointer_up()
        return user32.CallNextHookEx(
            None,
            nCode,
            wParam,
            ctypes.cast(lParam, ctypes.POINTER(ctypes.c_longlong)).contents,
        )

    def _window_event_callback(self, hwnd, msg, wparam, lparam):
        if msg == WM_INPUT:
            size = wintypes.UINT(0)
            res = user32.GetRawInputData(
                ctypes.cast(lparam, c_structs.PRAWINPUT),
                RID_INPUT,
                None,
                ctypes.byref(size),
                ctypes.sizeof(c_structs.RAWINPUTHEADER),
            )
            if res == wintypes.UINT(-1) or size == 0:
                return 0
            preparsed_data = ctypes.create_string_buffer(size.value)
            res = user32.GetRawInputData(
                ctypes.cast(lparam, c_structs.PRAWINPUT),
                RID_INPUT,
                preparsed_data,
                ctypes.byref(size),
                ctypes.sizeof(c_structs.RAWINPUTHEADER),
            )
            if res != size.value:
                return 0
            event = ctypes.cast(preparsed_data, c_structs.PRAWINPUT).contents
            self.handle_raw_hid_event(event)

        return DefWindowProcW(hwnd, msg, wparam, lparam)

    def _data_from_hid_event(self, hid_event):
        header = hid_event.header
        data = hid_event.data.hid
        size = wintypes.UINT(0)
        user32.GetRawInputDeviceInfoA(
            header.hDevice,
            RIDI_PREPARSEDDATA,
            None,
            ctypes.byref(size),
        )
        preparsed_data = ctypes.create_string_buffer(size.value)
        user32.GetRawInputDeviceInfoA(
            header.hDevice,
            RIDI_PREPARSEDDATA,
            preparsed_data,
            ctypes.byref(size),
        )
        xPos = ctypes.c_ulonglong()
        hid_parser.HidP_GetUsageValue(
            HidP_Input,
            0x01,
            0,
            0x30,
            ctypes.byref(xPos),
            preparsed_data,
            data.bRawData,
            data.dwSizeHid,
        )
        yPos = ctypes.c_ulonglong()
        hid_parser.HidP_GetUsageValue(
            HidP_Input,
            0x01,
            0,
            0x31,
            ctypes.byref(yPos),
            preparsed_data,
            data.bRawData,
            data.dwSizeHid,
        )
        numOfFingers = ctypes.c_ulonglong()
        hid_parser.HidP_GetUsageValue(
            HidP_Input,
            HID_USAGE_GENERIC_TOUCHPAD,
            0,
            HID_USAGE_DIGITIZER_CONTACT_COUNT,
            ctypes.byref(numOfFingers),
            preparsed_data,
            data.bRawData,
            data.dwSizeHid,
        )
        usage = wintypes.UINT(0)
        hid_parser.HidP_GetUsages(
            HidP_Input,
            HID_USAGE_GENERIC_TOUCHPAD,
            1,
            ctypes.byref(ctypes.c_void_p()),
            ctypes.byref(usage),
            preparsed_data,
            data.bRawData,
            data.dwSizeHid,
        )
        # caps = c_structs.HIDP_CAPS()
        # hid_parser.HidP_GetCaps(
        #     preparsed_data,
        #     ctypes.byref(caps),
        # )
        # asd = c_structs.HIDP_VALUE_CAPS()
        # hid_parser.HidP_GetValueCaps(
        #     HidP_Input,
        #     ctypes.byref(asd),
        #     caps.NumberOutputValueCaps,
        #     preparsed_data
        # )
        # print(caps)
        return (
            xPos.value,
            yPos.value,
            numOfFingers.value,
            usage.value == 1,
        )

    def handle_raw_hid_event(self, event) -> bool:
        """
        Handles a raw touchpad event.
        """
        x, y, amount_of_fingers, is_mouse_up = self._data_from_hid_event(event)
        if amount_of_fingers == 0:
            amount_of_fingers = self.current_amount_of_fingers
        if is_mouse_up:
            self.on_finger_up(amount_of_fingers)
        elif amount_of_fingers > self.current_amount_of_fingers:
            self.on_finger_down(amount_of_fingers)
        else:
            self.on_finger_move(x, y)
        self.current_amount_of_fingers = amount_of_fingers
        if amount_of_fingers == 1 and is_mouse_up:
            self.current_amount_of_fingers = 0

    def up_function(self):
        user32.mouse_event(
            0x0004,
            0,
            0,
            0,
            1,
        )

    def down_function(self):
        user32.mouse_event(
            0x0002,
            0,
            0,
            0,
            1,
        )

    @staticmethod
    def get_current_pointer_state():
        return user32.GetAsyncKeyState(
            0x01
        )  # 0x01 is the state of the left mouse button.

    @abstractmethod
    def on_finger_move(self, x, y):
        raise NotImplementedError

    @abstractmethod
    def on_finger_up(self, amount_of_fingers):
        raise NotImplementedError

    @abstractmethod
    def on_finger_down(self, amount_of_fingers):
        raise NotImplementedError

    @abstractmethod
    def on_blocking_pointer_down(self):
        raise NotImplementedError

    @abstractmethod
    def on_blocking_pointer_up(self):
        raise NotImplementedError
