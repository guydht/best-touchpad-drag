import ctypes
from ctypes import wintypes

wndproc_args = (wintypes.HWND, wintypes.UINT, wintypes.WPARAM, wintypes.LPARAM)

WNDPROC = ctypes.CFUNCTYPE(ctypes.c_ssize_t, *wndproc_args)
HOOKPROC = ctypes.CFUNCTYPE(ctypes.c_int, ctypes.c_int,wintypes.WPARAM, wintypes.LPARAM)

kernel32 = ctypes.WinDLL("Kernel32")
user32 = ctypes.WinDLL("User32")


class Struct(ctypes.Structure):
    def __str__(self) -> str:
        return " ".join([f"{field[0]}: {getattr(self, field[0])}" for field in self._fields_])
class HIDP_CAPS(Struct):
    _fields_ = [
        ("Usage", wintypes.USHORT),
        ("UsagePage", wintypes.USHORT),
        ("InputReportByteLength", wintypes.USHORT),
        ("OutputReportByteLength", wintypes.USHORT),
        ("FeatureReportByteLength", wintypes.USHORT),
        ("Reserved", wintypes.USHORT),
        ("NumberLinkCollectionNodes", wintypes.USHORT),
        ("NumberInputButtonCaps", wintypes.USHORT),
        ("NumberInputValueCaps", wintypes.USHORT),
        ("NumberInputDataIndices", wintypes.USHORT),
        ("NumberOutputButtonCaps", wintypes.USHORT),
        ("NumberOutputValueCaps", wintypes.USHORT),
        ("NumberOutputDataIndices", wintypes.USHORT),
        ("NumberFeatureButtonCaps", wintypes.USHORT),
        ("NumberFeatureValueCaps", wintypes.USHORT),
        ("NumberFeatureDataIndices", wintypes.USHORT)
    ]
class HIDP_VALUE_CAPS(Struct):
    _fields_ = [
        ("UsagePage", wintypes.USHORT),
        ("ReportID", ctypes.c_ubyte),
        ("DesignatorMin", wintypes.USHORT),
        ("DesignatorMax", wintypes.USHORT),
        ("DataIndexMin", wintypes.USHORT),
        ("DataIndexMax", wintypes.USHORT),
    ]

class WNDCLASSEXW(Struct):
    _fields_ = (
        ("cbSize", wintypes.UINT),
        ("style", wintypes.UINT),
        # ("lpfnWndProc", ctypes.c_void_p),
        ("lpfnWndProc", WNDPROC),
        ("cbClsExtra", ctypes.c_int),
        ("cbWndExtra", ctypes.c_int),
        ("hInstance", wintypes.HINSTANCE),
        ("hIcon", wintypes.HICON),
        ("hCursor", ctypes.c_void_p),
        ("hbrBackground", wintypes.HBRUSH),
        ("lpszMenuName", wintypes.LPCWSTR),
        ("lpszClassName", wintypes.LPCWSTR),
        ("hIconSm", wintypes.HICON),
    )


class RawInputDevice(Struct):
    _fields_ = (
        ("usUsagePage", wintypes.USHORT),
        ("usUsage", wintypes.USHORT),
        ("dwFlags", wintypes.DWORD),
        ("hwndTarget", wintypes.HWND),
    )


PRawInputDevice = ctypes.POINTER(RawInputDevice)


class RAWINPUTHEADER(Struct):
    _fields_ = (
        ("dwType", wintypes.DWORD),
        ("dwSize", wintypes.DWORD),
        ("hDevice", wintypes.HANDLE),
        ("wParam", wintypes.WPARAM),
    )


class BUTTONSTRUCT(Struct):
    _fields_ = (
        ("usButtonFlags", ctypes.c_ushort),
        ("usButtonData", ctypes.c_ushort),
    )


class RAWMOUSE(Struct):
    _fields_ = (
        ("usFlags", ctypes.c_ushort),
        ("ulButtons", BUTTONSTRUCT),
        ("ulRawButtons", ctypes.c_ulong),
        ("lLastX", ctypes.c_long),
        ("lLastY", ctypes.c_long),
        ("ulExtraInformation", ctypes.c_ulong),
    )


class RAWHID(Struct):
    _fields_ = (
        ("dwSizeHid", wintypes.DWORD),
        ("dwCount", wintypes.DWORD),
        ("bRawData", wintypes.BYTE * 1),
    )


class RAWINPUT_U0(Struct):
    _fields_ = (("hid", RAWHID),)


class RawInputData(ctypes.Union):
    _fields_ = (("hid", RAWHID), ("mouse", RAWMOUSE))


class RAWINPUT(Struct):
    _fields_ = (
        ("header", RAWINPUTHEADER),
        ("data", RawInputData),
    )


PRAWINPUT = ctypes.POINTER(RAWINPUT)
CreateWindowEx = user32.CreateWindowExW
CreateWindowEx.argtypes = (
    wintypes.DWORD,
    wintypes.LPCWSTR,
    wintypes.LPCWSTR,
    wintypes.DWORD,
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_int,
    wintypes.HWND,
    wintypes.HMENU,
    wintypes.HINSTANCE,
    wintypes.LPVOID,
)
