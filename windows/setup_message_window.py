import c_structs
from ctypes import wintypes
import ctypes

user32 = ctypes.WinDLL("User32")
kernel32 = ctypes.WinDLL("Kernel32")

GetModuleHandleW = kernel32.GetModuleHandleW
GetModuleHandleW.argtypes = (wintypes.LPWSTR,)
GetModuleHandleW.restype = wintypes.HMODULE

DefWindowProcW = user32.DefWindowProcW
DefWindowProcW.argtypes = c_structs.wndproc_args
DefWindowProcW.restype = ctypes.c_ssize_t

WM_QUIT = 0x0012
WM_TOUCH = 0x0240
WM_INPUT = 0x00FF
WM_KEYUP = 0x0101
WM_CHAR = 0x0102

WH_MOUSE_LL = 14

HID_USAGE_DIGITIZER_CONTACT_ID = 0x51
HID_USAGE_DIGITIZER_CONTACT_COUNT = 0x54

HID_USAGE_PAGE_GENERIC = 0x01
HID_USAGE_PAGE_DIGITIZER = 0x0D

RIDEV_NOLEGACY = 0x00000030
RIDEV_INPUTSINK = 0x00000100
RIDEV_CAPTUREMOUSE = 0x00000200

RID_HEADER = 0x10000005
RID_INPUT = 0x10000003

RIM_TYPEMOUSE = 0
RIM_TYPEKEYBOARD = 1
RIM_TYPEHID = 2

PM_NOREMOVE = 0x0000
RIDI_PREPARSEDDATA = 0x20000005


def register_devices(hwnd=None):
    flags = RIDEV_INPUTSINK
    devices = (c_structs.RawInputDevice * 1)(
        c_structs.RawInputDevice(
            HID_USAGE_PAGE_DIGITIZER,
            0x05,
            flags,
            hwnd,
        ),
    )
    if user32.RegisterRawInputDevices(
        devices, 1, ctypes.sizeof(c_structs.RawInputDevice)
    ):
        return True
    else:
        return False


def create_message_window(callback_for_events, callback_for_interrupting_events):
    wnd_cls = "asdasd"
    wcx = c_structs.WNDCLASSEXW()
    wcx.cbSize = ctypes.sizeof(c_structs.WNDCLASSEXW)
    wcx.lpfnWndProc = c_structs.WNDPROC(callback_for_events)
    wcx.hInstance = GetModuleHandleW(None)
    wcx.lpszClassName = wnd_cls
    user32.SetWindowsHookExW(7, )
    res = user32.RegisterClassExW(ctypes.byref(wcx))
    if not res:
        return 0
    hwnd = c_structs.CreateWindowEx(
        0, wnd_cls, None, 0, 0, 0, 0, 0, 0, None, wcx.hInstance, None
    )
    if not hwnd:
        return 0
    if not register_devices(hwnd):
        return 0
    hookProc = c_structs.HOOKPROC(callback_for_interrupting_events)
    g_mouseHook = user32.SetWindowsHookExW(WH_MOUSE_LL, hookProc, None, 0)
    if not g_mouseHook:
        return 0
    msg = wintypes.MSG()
    pmsg = ctypes.byref(msg)
    while res := user32.GetMessageW(pmsg, None, 0, 0):
        if res < 0:
            break
        user32.TranslateMessage(pmsg)
        user32.DispatchMessageW(pmsg)
    user32.UnhookWindowsHookEx(g_mouseHook)
