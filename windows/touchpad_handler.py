from basic_handler import Handler
from time import time


class TouchpadHandler(Handler):
    def __init__(self, *kargs, **kwargs):
        self.gesture_start_time = None
        self.two_fingers_start_time = None
        self.doing_custom_mouse_down_rn = False
        self.current_amount_of_fingers = 0
        self.fingers_position = dict()
        self.is_custom_down = (
            False  # Is the current event is a custom one triggered by me
        )
        self.is_custom_up = (
            False  # Is the current event is a custom one triggered by me
        )
        super().__init__(*kargs, **kwargs)

    def on_finger_up(self, amount_of_fingers):
        amount_of_fingers = amount_of_fingers or self.current_amount_of_fingers
        if amount_of_fingers == 1 and self.doing_custom_mouse_down_rn:
            self.doing_custom_mouse_down_rn = False
            self.up_function()
        elif amount_of_fingers == 2 and self.doing_custom_mouse_down_rn:
            self.doing_custom_mouse_down_rn = False
            self.up_function()
        elif (
            amount_of_fingers == 2
            and self.two_fingers_start_time
            and self.gesture_start_time
            and self.two_fingers_start_time - self.gesture_start_time > 0.1
            and time() - self.two_fingers_start_time < 0.2
        ):
            self.doing_custom_mouse_down_rn = True
        if amount_of_fingers == 1:
            self.gesture_start_time = None
        elif amount_of_fingers == 2:
            self.two_fingers_start_time = None

    def on_finger_down(self, amount_of_fingers):
        if amount_of_fingers == 1:
            self.gesture_start_time = time()
        elif amount_of_fingers == 2:
            self.two_fingers_start_time = time()

    def on_finger_move(self, x, y):
        # print(x, y)
        is_mouse_down_rn = self.get_current_pointer_state() != 0
        if self.doing_custom_mouse_down_rn:
            if not is_mouse_down_rn:
                self.down_function()

    def on_blocking_pointer_up(self) -> bool:
        """
        Function which is called when a "pointer up" event is triggered by windows

        Its return value is whether or not it should block the current pointer up event.
        """
        if (
            self.current_amount_of_fingers == 1
            and not self.is_custom_up
            and self.doing_custom_mouse_down_rn
        ):
            return True
        return False

    def on_blocking_pointer_down(self) -> bool:
        """
        Function which is called when a "pointer down" event is triggered by windows

        Its return value is whether or not it should block the current pointer down event.
        """
        if self.current_amount_of_fingers == 2 and not self.is_custom_down:
            return True
        return False

    def up_function(self):
        self.is_custom_up = True
        super().up_function()
        self.is_custom_up = False

    def down_function(self):
        self.is_custom_down = True
        super().down_function()
        self.is_custom_down = False


if __name__ == "__main__":
    TouchpadHandler().run()
