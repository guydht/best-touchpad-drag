from touchpad_handler import TouchpadHandler
from infi.systray import SysTrayIcon
from pathlib import Path


if __name__ == "__main__":
    handler = TouchpadHandler()
    with SysTrayIcon(
        str(Path(__file__).parent.joinpath(Path("OIG.ico"))),
        "Better Touchpad",
        on_quit=handler.stop,
    ) as systray:
        handler.run()
